package fi.metropolia.shipwar.fragments;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import fi.metropolia.shipwar.R;
import fi.metropolia.shipwar.ShipContainerImageAdapter;
import fi.metropolia.shipwar.core.GameSessionManager;
import fi.metropolia.shipwar.core.Player;
import fi.metropolia.shipwar.core.Ship;
import fi.metropolia.shipwar.delegates.EventsDelegate;
import fi.metropolia.shipwar.delegates.SessionManagerEventsDelegate;

public class GamePageFragment extends Fragment implements SessionManagerEventsDelegate{
	public View v;
	public EventsDelegate delegate;
	public Resources res;
	public Bundle shipBundle;
	private GameSessionManager manager;
	private GamePageFragment self;
	
	//For game sounds
	 private SoundPool soundPool;
     private int soundID;
     boolean plays = false, loaded = false;
     private float volume;
     AudioManager audioManager;
     int counter;
     
     private int targetMissSound;
     private int targetHitSound;
     private int shipMoveSound;
     private int gridSelectionSound;
    //sound ends here 
	//tracks the selection position of the grid
	private int selectedAttackIndex;
	private int selectedMoveIndex;
	
	//this is for letting user tap the confirm button once in one try
	private boolean confirmButtonEnabled;
	
	//Image resource for different states
	private int rockResource;
	private int opponentRockResource;
	private int gridSquareResource;
	private int gridSquareMoveableResource;
	private int opponentGridSquareResource;
	
	//Layout views
	private GridView shipContainer;
	private TextView activePlayerShip;
	private ProgressBar activePlayerHealthBar;
		//Player1 is manual and Player2 is automatic
	private ImageView player1Indicator;
	private ImageView player2Indicator;
	
	private int randomUse;
	private boolean randomBoolean;
	private ImageView randomImageView;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		v = inflater.inflate(R.layout.game_page, container,false);
		res=getResources();
		self=this;
		//Get arguments for selected ship
		shipBundle=getArguments();
		loadGameEnvironment();
		attachEventHandlers();
		new Handler().postDelayed(new Runnable(){
			public void run(){
				drawMap(manager.manualPlayer().ship().getPosition(),manager.manualPlayer().getRockPositions());
			}
		}, 50);
		
		return v;
	}
	public void onAttach(Activity activity){
		super.onAttach(activity);
		try{
			delegate=(EventsDelegate) activity;
		}
		catch(ClassCastException e){
			throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
		}
	}
	public void onBackPressed(){
		//super.onBackPressed();
		Toast.makeText(getActivity(), "Back", Toast.LENGTH_SHORT).show();
	}
	
	public void loadGameEnvironment(){
		//assign rockImage resource and grid square background resource
		this.gridSquareResource=R.drawable.game_page_grid_square;
		this.opponentGridSquareResource=R.drawable.game_page_opponent_grid_square;
		this.rockResource = R.drawable.game_page_rocks;
		this.opponentRockResource=R.drawable.game_page_opponent_rocks;
		this.gridSquareMoveableResource=R.drawable.game_page_grid_square_moveable;
		
		//Sounds
		this.audioManager=(AudioManager) getActivity().getSystemService(getActivity().AUDIO_SERVICE);
		
		float actualVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		float maxVolume = (float) audioManager
				.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		this.volume = actualVolume/maxVolume;
		this.soundPool=new SoundPool(10,AudioManager.STREAM_MUSIC,0);
		this.soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {
			@Override
			public void onLoadComplete(SoundPool soundPool, int sampleId,int status) {
				loaded = true;
			}
		});
		this.targetMissSound=this.soundPool.load(getActivity(),R.raw.game_page_target_miss,1);
		this.shipMoveSound=this.soundPool.load(getActivity(),R.raw.game_page_ship_moving,1);
		this.gridSelectionSound=this.soundPool.load(getActivity(),R.raw.game_page_grid_select_sound,1);
		this.targetHitSound=this.soundPool.load(getActivity(),R.raw.game_page_target_hit,2);
		
		this.confirmButtonEnabled=true;
		this.selectedAttackIndex=1;
		//Manual
		Drawable mShipImage=res.obtainTypedArray(R.array.ship_images).getDrawable(shipBundle.getInt("shipIndex"));
		Ship manualShip=new Ship(mShipImage,shipBundle.getString("name"),shipBundle.getString("desc"),shipBundle.getString("health"),shipBundle.getString("movement"));
		Player player=new Player("Player",manualShip,true);
		this.selectedMoveIndex=manualShip.getMoveablePositions()[0];
		mShipImage=null;	
		
		//Ship automatic needs to be redone
			Ship autoShip=new Ship(res.getDrawable(R.drawable.battleshipa),"Auto","this is cool");
			Player comp=new Player("Computer",autoShip,false);
	
			
			//Map container
				FrameLayout container=(FrameLayout)v.findViewById(R.id.map_container);
				container.setPadding(container.getWidth(),container.getHeight(),container.getWidth(),container.getHeight());
			
			manager=new GameSessionManager(new Player[]{player,comp});
				manager.setManualPlayer(player);
				manager.registerDelegate(this);
				
				new Handler().postDelayed(new Runnable(){
					public void run(){
						self.manager.gameStatus=true;
						self.manager.decideTurn();
					}
				}, 50);
		
			this.activePlayerHealthBar=(ProgressBar)v.findViewById(R.id.game_page_active_player_health_bar);
			this.activePlayerShip=(TextView)v.findViewById(R.id.game_page_active_player_ship);
			this.player1Indicator=(ImageView)v.findViewById(R.id.game_page_player1_indicator);
			this.player2Indicator=(ImageView)v.findViewById(R.id.game_page_player2_indicator);
		
			//player1 is manual and player2 is automatic 
	}
	
	public void attachEventHandlers(){
		shipContainer=(GridView)v.findViewById(R.id.ship_container);
		shipContainer.setAdapter(new ShipContainerImageAdapter(getActivity()));
		shipContainer.setOnItemClickListener(new OnItemClickListener() {
	        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
	        	if (manager.gameStatus && manager.attacker.getControlMode()){
	        		if(manager.attacker.getCurrentMode() && position != selectedAttackIndex){
	        			playSound(self.gridSelectionSound);
	        			clearLastSelected(selectedAttackIndex);
	        			selectedAttackIndex=position;
	        			markSelection(selectedAttackIndex,Color.RED);
	        		}
	        		else if (!manager.attacker.getCurrentMode() && contains(manager.attacker.ship().getMoveablePositions(),position) 
	        				&& position != selectedMoveIndex){
	        			playSound(self.gridSelectionSound);
	        			clearLastSelected(selectedMoveIndex);
	        			self.selectedMoveIndex=position;
	        			markSelection(selectedMoveIndex,Color.BLUE);
	        		}
	        		
	        	}
	        }
	    });
		
			
		Button confirmButton=(Button)v.findViewById(R.id.game_page_confirm_button);
		confirmButton.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v){
				if (self.confirmButtonEnabled && manager.gameStatus && manager.attacker.getControlMode()){
					if (manager.attacker.getCurrentMode()){
						manager.attacker.action(selectedAttackIndex);
					}else
					{
						manager.attacker.action(selectedMoveIndex);
					}
					self.confirmButtonEnabled=false;
				}
					
			}
		});
		
		
		Button attackModeButton=(Button)v.findViewById(R.id.game_page_attack_mode_button);
		attackModeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (manager.gameStatus && manager.attacker.getControlMode() && !manager.attacker.getCurrentMode()){
					manager.attacker.setCurrentMode(true);
				}
			}
		});
		
		Button moveModeButton=(Button)v.findViewById(R.id.game_page_defence_mode_button);
		moveModeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (manager.gameStatus && manager.attacker.getControlMode() && manager.attacker.getCurrentMode()){
					manager.attacker.setCurrentMode(false);
				}
			}
		});
	}
	
	
	
	
/*
 		* DELEGATE EVENT HANDLERS
*/
	
//Triggered when the computer attack 
		public void HighlightAttackedPosition(int position){
			ImageView selected=(ImageView)shipContainer.getChildAt(position);
			selected.setImageResource(R.drawable.game_page_explosion);
			if(position == manager.manual.ship().getPosition()){
				selected.setBackground(manager.manual.ship().getImage());
			}
			//selected.setBackgroundResource(R.drawable.game_page_explosion);
			AnimationDrawable frameAnimation=(AnimationDrawable)selected.getDrawable();
				frameAnimation.start();
			this.randomUse=position;
			this.randomImageView=selected;
			new Handler().postDelayed(new Runnable(){
				public void run(){
					if (manager.manual.ship().getPosition() == self.randomUse){
						self.randomImageView.setImageDrawable(manager.manual.ship().getImage());
					}else{
						self.randomImageView.setImageResource(R.drawable.transparent);
					}
				}
			}, 1000);
		}
//Triggered when next players is switched
		public void PlayerSwitched(){
			if (manager.attacker.getControlMode()){
				this.player1Indicator.setImageResource(R.drawable.game_page_player1_button_depressed);
				this.player2Indicator.setImageResource(R.drawable.game_page_player2_button);
				clearMoveables(manager.manual.ship().lastMoveablePositions);
				markMoveables(manager.manual.ship().getMoveablePositions());
			}
			else{
					this.player2Indicator.setImageResource(R.drawable.game_page_player2_button_depressed);
					this.player1Indicator.setImageResource(R.drawable.game_page_player1_button);
				new Handler().postDelayed(new Runnable(){
					public void run(){
						if (manager.gameStatus) {
							manager.attacker.action(-1);
						}
					}
				}, 500);
			}
			this.activePlayerShip.setText(manager.attacker.ship().getName());
			self.confirmButtonEnabled=true;
			setLife(manager.attacker.ship().getLife());
		}
//Triggered when a player changes the mode
		public void ModeChanged(boolean mode){
			randomBoolean=mode;
			if (manager.attacker.getControlMode()){
				self.switchMap(mode);
				
			}
		}
		
//Triggered when a player changes the position
		public void PositionChanged(int old, int newPosition){
			playSound(this.shipMoveSound);
			if (manager.attacker.getControlMode()){
				new Handler().postDelayed(new Runnable(){
					public void run(){
						clearMoveables(manager.attacker.ship().lastMoveablePositions);
					}
				}, 30);
					
			}
			ImageView imageView=(ImageView)shipContainer.getChildAt(old);
				imageView.setImageResource(R.drawable.transparent);
				
				imageView=(ImageView)shipContainer.getChildAt(newPosition);
			
			imageView.setImageDrawable(manager.manual.ship().getImage());
			imageView=null;
		}
		
//Triggered whenever a player attacks
		public void UpdateInfoUI(Boolean controlMode, int[] values,boolean hit){
			if (hit){
				playSound(this.targetHitSound);
					if (manager.opponent.ship().getLife() <= 0){
						self.gameOver(manager.attacker);
					}
			}else{
				playSound(this.targetMissSound);
			}
		}
		
 /*
  		* HELPER FUNCTIONS
  */
//called when the game is over
	public void gameOver(Player winner){
		manager.gameStatus=false;
		self.v.setAlpha(0.9f);
		delegate.GameOver(winner);
	}
	
//play sound effects	
    public void playSound(int soundID){
    	this.soundPool.play(soundID, 1, 1, 1, 0, 1f);
    }
    
//switch map when the mode is changed by the manual player
	public void switchMap(Boolean mode){
		if (mode)
		{
			clearMoveables(manager.attacker.ship().getMoveablePositions());
			clearMap(manager.attacker.ship().getPosition());
			drawMap(0,manager.opponent.getRockPositions());
			clearLastSelected(selectedMoveIndex);
			markSelection(selectedAttackIndex,Color.RED);
		}
		else
		{
			clearMap(0);
			markMoveables(manager.attacker.ship().getMoveablePositions());
			drawMap(manager.attacker.ship().getPosition(),manager.attacker.getRockPositions());
			clearLastSelected(self.selectedAttackIndex);
			markSelection(selectedMoveIndex,Color.BLUE);
		}
	}
		
//clear the manual ship from the grid	&& if shipPosition == 0 then it is for computer	
	private boolean clearMap(int shipPosition)
	{
		boolean reValue=true;
		//clearing every grid
		for (int count=0;count < shipContainer.getAdapter().getCount();count++)
		{
			View v = shipContainer.getChildAt(count);
			if (shipPosition != 0 )//)
			{
				v.setBackgroundResource(self.opponentGridSquareResource);
				v.setAlpha(0.9f);
			}
			else if (shipPosition == 0)//&& !contains(rocks,count))
			{
				v.setBackgroundResource(self.gridSquareResource);
				v.setAlpha(1f);
			}
			v=null;
		}
		
		ImageView imageView=(ImageView)shipContainer.getChildAt(shipPosition);
			imageView.setImageResource(R.drawable.transparent);
			imageView=null;
			
		
		return reValue;
	}
// draw the rocks and ship in the grid	
	private void drawMap(int shipPosition,int[] rocks)
	{
		for (int i=0;i<rocks.length;i++){
			if (rocks[i] >= 0 && rocks[i] < shipContainer.getAdapter().getCount()){
				shipContainer.getChildAt(rocks[i]).setBackgroundResource(self.rockResource);
				shipContainer.getChildAt(rocks[i]).setAlpha(1f);
			}
		}
		if (shipPosition != 0){
			ImageView imageView=(ImageView)shipContainer.getChildAt(shipPosition);
			imageView.setImageDrawable(manager.manual.ship().getImage());
			imageView=null;
		}
	}
//draw the moveables position indicator	
	private void markMoveables(int[] positions){
		for (int position:positions){
			if (position >= 0 && position < self.shipContainer.getAdapter().getCount()){
				shipContainer.getChildAt(position).setBackgroundResource(self.gridSquareMoveableResource);
			};
		}
	}
	
//clear the moveables position indicator	
	private void clearMoveables(int[] positions){
		for (int position: positions){
			if (position >= 0 && position < shipContainer.getAdapter().getCount()){
				if (contains(manager.manual.getRockPositions(),position)){
					shipContainer.getChildAt(position).setBackgroundResource(self.rockResource);
				}else{
					shipContainer.getChildAt(position).setBackgroundResource(self.gridSquareResource);
				}
			}
		}
	}
//helper to find out if an array contains the item	
	private boolean contains(int[] positions,int position){
		for(int item:positions){
			if(item==position){
				return true;
			}
		}
		return false;
	}
	
//set the life to the progress bar
	private void setLife(int health){
		this.activePlayerHealthBar.setProgress(health);
	}
//clear the selection indicator of the grid when another grid is selected
	private void clearLastSelected(int lastSelected){
		ImageView selectedView=(ImageView)shipContainer.getChildAt(lastSelected);
		if (manager.manual.ship().getPosition() != lastSelected){
			selectedView.setImageResource(R.drawable.transparent);
		}
	}
//draw the selection indicator
	private void markSelection(int selectedIndex,int color){
		ColorDrawable cd=new ColorDrawable(color);
		cd.setAlpha(100);
		ImageView selectedView=(ImageView)shipContainer.getChildAt(selectedIndex);
		selectedView.setImageDrawable(cd);
	}

}
