package fi.metropolia.shipwar.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import fi.metropolia.shipwar.R;
import fi.metropolia.shipwar.delegates.EventsDelegate;

public class MainPageFragment extends Fragment {
	
	EventsDelegate delegate;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
   
		View v=inflater.inflate(R.layout.main_page, container,false);
		
		Button option_button=(Button)v.findViewById(R.id.main_page_option_button);
		Button start_button=(Button)v.findViewById(R.id.main_page_start_button);
		option_button.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {
               //Toast.makeText(getActivity(), "Options", Toast.LENGTH_SHORT).show();     
            }
       });
		
		start_button.setOnClickListener(new OnClickListener(){
			@Override 
			public void onClick(View v)
			{
				delegate.NewGame();
				//Toast.makeText(getActivity(), "Start", Toast.LENGTH_SHORT).show();
			}
		});
		
		
		return v;
    }
	
	@Override
	
	public void onAttach(Activity activity){
		super.onAttach(activity);
		try{
			delegate=(EventsDelegate) activity;
		}
		catch(ClassCastException e){
			throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
		}
	}
	
}
