package fi.metropolia.shipwar.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import fi.metropolia.shipwar.R;
import fi.metropolia.shipwar.delegates.EventsDelegate;

public class GameOverFragment extends Fragment {
	EventsDelegate delegate;
	View v;
	@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
		        Bundle savedInstanceState)
		{
			v=inflater.inflate(R.layout.game_over_dialog_fragment, container, false);;
			Bundle bundle=getArguments();
				String winner_name= bundle.getString("player");
			TextView name=(TextView)v.findViewById(R.id.game_over_fragment_congratulations);
				name.setText(winner_name);
				
				
			Button retryButton=(Button)v.findViewById(R.id.game_over_fragment_retry_button);
			retryButton.setOnClickListener(new View.OnClickListener(){
				public void onClick(View v){
					delegate.NewGame();
				}
			});
			Button exitButton=(Button)v.findViewById(R.id.game_over_fragment_exit_button);
			exitButton.setOnClickListener(new View.OnClickListener(){
				public void onClick(View v){
					System.exit(0);
				}
			});
			
			return v;
		}
	
		public void onAttach(Activity activity){
			super.onAttach(activity);
			try{
				delegate=(EventsDelegate) activity;
			}
			catch(ClassCastException e){
				throw new ClassCastException(activity.toString()
	                    + " must implement OnHeadlineSelectedListener");
			}
		}
}
