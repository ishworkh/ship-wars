package fi.metropolia.shipwar.fragments;

import android.app.Activity;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import fi.metropolia.shipwar.R;
import fi.metropolia.shipwar.delegates.EventsDelegate;

public class SelectShipsFragment extends Fragment{
	public View v;
	EventsDelegate delegate;
	//Resources
		private TypedArray titles;
		private TypedArray descriptions;
		private TypedArray images;
		private TypedArray healths;
		private TypedArray movements;
		
	//Views
		private LinearLayout mainContainer;
		private ImageView imageContainer;
		private TextView titleContainer;
		private TextView descriptionContainer;
		private TextView healthContainer;
		private TextView movementContainer;
	//Gestures
		private GestureDetector gestureDetector;
	//
		private int currentIndex=0;
		private SelectShipsFragment self;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		v=inflater.inflate(R.layout.select_ship_page, container,false);
		self=this;
		Resources res=getResources();
			titles=res.obtainTypedArray(R.array.ship_titles);
			descriptions=res.obtainTypedArray(R.array.ship_descriptions);
			images=res.obtainTypedArray(R.array.ship_images);
			healths=res.obtainTypedArray(R.array.ship_healths);
			movements=res.obtainTypedArray(R.array.ship_movements);
		//Views
			mainContainer=(LinearLayout)v.findViewById(R.id.select_ships_main_container);
			imageContainer=(ImageView)v.findViewById(R.id.select_ship_page_ship_image);
			titleContainer=(TextView)v.findViewById(R.id.select_ship_page_ship_title);
			descriptionContainer=(TextView)v.findViewById(R.id.select_ship_page_ship_desc);
			healthContainer=(TextView)v.findViewById(R.id.select_ship_page_ship_health);
			movementContainer=(TextView)v.findViewById(R.id.select_ship_page_ship_movement);
		
			
		final Button start_button=(Button)v.findViewById(R.id.select_ship_page_start_button);
		
		//set the first ship info
			this.changeShip(this.currentIndex);
		
		//Gesture detector
			gestureDetector=new GestureDetector(getActivity(),new GestureDetector.SimpleOnGestureListener(){
				
				public boolean onDown(MotionEvent e){
					return true;
				}
				
				public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX,float velocityY ){
			    	try {
                        if (e1.getX() - e2.getX() > 0) {
                        	self.swipeLeft();
                        } else {
                        	self.swipeRight();                        
                        }
                    } catch (Exception e) {
                        // nothing
                    }
                    return super.onFling(e1, e2, velocityX, velocityY);
				}
			});
			
			v.setOnTouchListener(new View.OnTouchListener() {
				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					return self.gestureDetector.onTouchEvent(event);
				}
			});
			
		start_button.setOnClickListener(new OnClickListener(){
			@Override 
			public void onClick(View view)
			{
				delegate.StartGame(self.currentIndex,
						self.titles.getString(self.currentIndex),
						self.descriptions.getString(self.currentIndex),
						self.healths.getString(self.currentIndex),
						self.movements.getString(self.currentIndex)
						);
			}
			
		});
		return v;
	}
	
	public void onAttach(Activity activity){
		super.onAttach(activity);
		try{
			delegate=(EventsDelegate) activity;
		}
		catch(ClassCastException e){
			throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
		}
	}
	
	public void changeShip(int pos)
	{
			this.imageContainer.setImageDrawable(this.images.getDrawable(pos));
			this.titleContainer.setText(this.titles.getString(pos));
			this.descriptionContainer.setText(this.descriptions.getString(pos));
			this.healthContainer.setText(this.healths.getString(pos));
			this.movementContainer.setText(this.movements.getString(pos));
	}
	
	public void swipeLeft()
	{
		//int fromXType, float fromXValue, int toXType, float toXValue, int fromYType, float fromYValue, int toYType, float toYValue)
		int delay=100;
		TranslateAnimation anim=new TranslateAnimation(Animation.RELATIVE_TO_SELF,1,Animation.RELATIVE_TO_PARENT,0,Animation.RELATIVE_TO_PARENT,0,Animation.RELATIVE_TO_SELF,0);
	
		if (this.currentIndex < this.titles.length()-1){
	    	   this.imageContainer.startAnimation(anim);
	    	   this.currentIndex++;
	    	   new Handler().postDelayed(new Runnable(){
	   			public void run(){
	   				self.changeShip(self.currentIndex);
	   			}
	   		}, 0);
	       }
      
   }
	
	public void swipeRight(){
		int delay=100;
		TranslateAnimation anim=new TranslateAnimation(Animation.RELATIVE_TO_SELF,-1,Animation.RELATIVE_TO_SELF,0,Animation.RELATIVE_TO_SELF,0,Animation.RELATIVE_TO_SELF,0);
		anim.setDuration(delay);		//
		if (this.currentIndex > 0){
			this.imageContainer.startAnimation(anim);
			this.currentIndex--;
			 new Handler().postDelayed(new Runnable(){
					public void run(){
						self.changeShip(self.currentIndex);
					}
				}, 0);
		}
	}
}
