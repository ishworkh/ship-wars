package fi.metropolia.shipwar.delegates;
//Events delegate from player class to gamesessionManager class
public interface PlayerEventsDelegate {
	public void ModeChanged(boolean mode);
	public void Attacked(int position);
	public void PositionChanged();
}
