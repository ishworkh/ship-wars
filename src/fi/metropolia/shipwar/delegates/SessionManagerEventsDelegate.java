package fi.metropolia.shipwar.delegates;
// event delegate from gameSessionManager to fragment UI
// reponsible for update UI part of the game
public interface SessionManagerEventsDelegate {
	public void PlayerSwitched();
	public void ModeChanged(boolean mode);
	public void PositionChanged(int oldPosition,int newPosition);
	public void UpdateInfoUI(Boolean controlMode,int[] values,boolean suceeded);
	public void HighlightAttackedPosition(int position);
}
