package fi.metropolia.shipwar.delegates;

import fi.metropolia.shipwar.core.Player;

//Events Delegate from Fragments to the main Activity
public interface EventsDelegate {
	//Main Fragment Events
		public void NewGame();
		
	//Select Ship Fragment Events	
		public void StartGame(int imageIndex, String desc,String name,String health,String movement);
		
	//Game Page Fragment events
		public void GameOver(Player winner);
}
