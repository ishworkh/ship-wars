package fi.metropolia.shipwar;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ShipContainerImageAdapter extends BaseAdapter {
	private Context mContext;

    public ShipContainerImageAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return 98;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            int width=parent.getWidth()/14;
            int height=parent.getHeight()/7;
            if (position == 0){
            	 imageView.setLayoutParams(new GridView.LayoutParams(LayoutParams.WRAP_CONTENT,30));
            }else{
                imageView.setLayoutParams(new GridView.LayoutParams(width,height));
            }
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(0, 0, 0, 0);
        } 
        else {
            imageView = (ImageView) convertView;
        }
        	imageView.setImageResource(R.drawable.transparent);
        	imageView.setBackgroundResource(R.drawable.game_page_grid_square);
        	return imageView;
    }

    // references to our images
   
}
