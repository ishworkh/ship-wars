package fi.metropolia.shipwar.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fi.metropolia.shipwar.delegates.PlayerEventsDelegate;

public class Player {
	final private String name;
	protected List<Ship> ships;
	protected Boolean controlMode; //false - automatic ||true - manual
	protected Boolean currentMode = false; //true - attack mode || false - defence mode
	protected int[] rocksPosition;
	
	protected Boolean lastAttackSuccess=false;
	protected Boolean lastHit=false;
	
	protected Boolean opponentLastCall=false;
	protected Boolean ownLastCall=false;
	
	public int attackAttempts=0;
	public int successAttempts=0;
	public int defenceAttempts=0;
	
	PlayerEventsDelegate playerEvents;
	
	
	public Player(String name, List<Ship> ships,Boolean mode){
		this.name=name;
		this.ships=ships;
		this.controlMode=mode;
		this.rocksPosition=calculateRockPositions();
	}
	
	public Player(String name, Ship ship,Boolean mode){
		this.ships=new ArrayList<Ship>();
		this.name=name;
		this.ships.add(ship);
		this.controlMode=mode;
		this.rocksPosition=calculateRockPositions();
	}
	
	public void registerDelegate(PlayerEventsDelegate playerEvents)
	{
		this.playerEvents=playerEvents;
	}
	public int[] getRockPositions()
	{
		return this.rocksPosition;
	}
	
	public Boolean getCurrentMode(){
		return this.currentMode;
	}
	
	public void setCurrentMode(Boolean mode){
		this.currentMode=mode;
		playerEvents.ModeChanged(this.currentMode);
	}
	
	
	
	public String getName(){
		return this.name;
	}
	
	public void action(int position){
		this.currentMode=(position == -1)?decideMode(opponentLastCall):this.currentMode;
		if (this.currentMode){
			this.attack(position);
		}else{
			this.changePosition(position);
		}
	}

 	
	public void changePosition(int position){
		position=(position == -1)?calcChangePosition(opponentLastCall):position;
		if (!this.currentMode){
			this.ship().setPosition(position);
			this.defenceAttempts++;
			this.ownLastCall=false;
			playerEvents.PositionChanged();
		}
	}
	
	public void attack(int position){
		position=(position == -1)?calcAttackPosition(opponentLastCall):position;
		if (this.currentMode && this.ship().attack(position))
		{	
			this.attackAttempts++;
			this.ownLastCall=true;
			playerEvents.Attacked(position);
		}
	}
	
	public Boolean getControlMode(){
		return this.controlMode;
	}
	
	
	public Ship ship(){
		return this.ships.get(0);
	}
	
	public void setLastAttackSuccess(boolean success){
		if (success){
			this.lastAttackSuccess=success;
			this.successAttempts++;
		}else{
			this.lastAttackSuccess=false;
		}
		
	}
	public boolean lastAttackSucceeded(){
		return this.lastAttackSuccess;
	}
	
	public void setOpponentLastCall(Boolean lastCall){
		this.opponentLastCall=lastCall;
	}
	public void setLastHit(Boolean lasthit){
		this.lastHit=lasthit;
	}
	
//Helper methods
//calculate rock positions
	protected int[] calculateRockPositions(){
		int times=new Random().nextInt(5)+1;
		int[] pos=new int[times];
		for (int i=0;i<times;i++)
		{
			pos[i]=new Random().nextInt(98)+1;
		}
		return pos;
	}
//calculate mode for computer	
		private boolean decideMode(boolean opponentLastCall){
			if (opponentLastCall && this.lastHit){
				playerEvents.ModeChanged(false);
				return false;
			}
			playerEvents.ModeChanged(true);
			return true;
			
		}
		
//calculates attack position for computer		
		private int calcAttackPosition(boolean opponentLastCall){
			if (opponentLastCall && this.lastAttackSuccess){
				return this.ship().getLastAttackPosition();
			}
			return genRand();
		}
		
// calculates move position for computer		
		private int calcChangePosition(boolean opponentLastCall){
			if (opponentLastCall && this.lastHit){
				//int rand=genRand();
				int rand = new Random().nextInt(this.ship().getMoveablePositions().length-1)+0;
				while (this.ship().getMoveablePositions()[rand] == this.ship().getPosition()){
					rand=new Random().nextInt(this.ship().getMoveablePositions().length-1)+0;
				}
				return rand;
			}
			
			return this.ship().getMoveablePositions()[genRand(0,this.ship().getMoveablePositions().length)];
		}
		
		
		
		private int genRand(int lowerRange,int upperRange){
			return new Random().nextInt(upperRange-lowerRange)+ lowerRange;
		}
		
		private int genRand(){
			return new Random().nextInt(98)+0;
		}
	
	
	
	
}
