package fi.metropolia.shipwar.core;

import java.util.Arrays;
import java.util.Random;

import android.graphics.drawable.Drawable;

public class Ship {
	private Drawable image;
	private String name;
	private String desc;
	private int life;
	private int position;
	private int lastPosition;
	private int lastAttackPosition;
	
	private int health;
	private int movementRange;
	public int[] lastMoveablePositions;
	private int[] moveablePositions;
	
	public Ship(Drawable image, String name,String desc,String health, String movementRange){
		this.image=image;
		this.name=name;
		this.desc=desc;
		this.life=100;
		this.health=(health == "Low")?2:1;//Health can be high or low
		this.movementRange=(movementRange=="Fast")?3:2;//movement can be fast = 6 steps and slow= 4 steps
		this.position=new Random().nextInt(97)+1;
		this.lastPosition=this.position;
		this.lastAttackPosition=-1;
		this.moveablePositions=calculateMoveablePositions(this.position);
		this.lastMoveablePositions=new int[this.moveablePositions.length];
	}
	
	public Ship(Drawable image, String name,String desc){
		this.image=image;
		this.name=name;
		this.desc=desc;
		this.life=100;
		this.position=new Random().nextInt(97)+1;
		this.lastPosition=this.position;
		this.lastAttackPosition=-1;
		this.moveablePositions=calculateMoveablePositions(this.position);
		this.lastMoveablePositions=new int[this.moveablePositions.length];
	}
	
	public void setLife(int life){
		this.life=life;
	}
	
	public int getLife(){
		return this.life;
	}
	
	public void decreaseLife(int ratio){
		this.setLife(getLife() - getHealth() * ratio);
	}
	
	public void setHealth(int health){
		this.health= health;
		
	}
	
	public int getHealth()
	{
		return this.health;
	}
	
	public void setMovementRange(int range){
		this.movementRange= range;
		
	}
	
	public int getMovementRange()
	{
		return this.movementRange;
	}
	
	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return this.name;
	}

	public void setDesc(String desc){
		this.desc=desc;
	}
	public String getDesc(){
		return this.desc;
	}
	
	public void setImage(Drawable image){
		this.image=image;
	}
	public Drawable getImage(){
		return this.image;
	}
	
	public void setPosition(int position){
		if (contains(this.moveablePositions,position)){
			this.lastPosition=this.position;
			this.position=position;
			this.lastMoveablePositions=this.moveablePositions;
			this.moveablePositions=calculateMoveablePositions(this.position);
		}
	}
	public int getPosition(){
		return this.position;
	}
	
	public int getLastPosition(){
		return this.lastPosition;
	}
	
	public int getLastAttackPosition(){
		return this.lastAttackPosition;
	}

	public boolean attack(int attackPosition){
		this.lastAttackPosition=attackPosition;
		return true;
	}
	public int[] getMoveablePositions(){
		return this.moveablePositions;
	}
	//
	private boolean contains(int[] positions,int position){
		for(int item:positions){
			if(item==position){
				return true;
			}
		}
		return false;
	}
	
	private int[] calculateMoveablePositions(int currentPosition){
		int count=0;
		int[] retVal=new int[20];
			Arrays.fill(retVal, -1);
		for(int i = 1;i <= this.movementRange;i++){
			if (Math.ceil(currentPosition /14) == Math.ceil((currentPosition - i)/14)){
				retVal[count] = currentPosition-i;
				count++;
			}
			if (Math.ceil(currentPosition /14) == Math.ceil((currentPosition + i)/14)){
				retVal[count]=currentPosition + i;
				count++;
			}
			retVal[count] = currentPosition - (14*i);	
				count++;
			retVal[count] = currentPosition + (14*i);
				count++;
		}
		return retVal;
	}
	
}
