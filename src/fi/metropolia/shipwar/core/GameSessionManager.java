package fi.metropolia.shipwar.core;

import java.util.Random;

import android.os.Handler;
import fi.metropolia.shipwar.delegates.PlayerEventsDelegate;
import fi.metropolia.shipwar.delegates.SessionManagerEventsDelegate;
public class GameSessionManager implements PlayerEventsDelegate{
	public Player attacker;
	public Player opponent;
	public Player manual;
	public int computer;
	public boolean gameStatus;//true for running
	private Player[] players;
	private GameSessionManager self;
	SessionManagerEventsDelegate delegate;
	
	public void registerDelegate(SessionManagerEventsDelegate delegate){
		this.delegate=delegate;
	}

	public GameSessionManager(Player[] players)
	{	
		self=this;
		this.players=players;
		this.gameStatus=false;
		for (int i=0;i<players.length;i++)
		{
			players[i].registerDelegate(this);
		}
	}
	
	
	public void decideTurn(){
		//int attackerIndex=decideFirstTurn(2);
		if (players[0].getControlMode()){
			this.attacker=players[0];
			this.opponent=players[1];
		}
		else{
			
			this.attacker=players[1];
			this.opponent=players[0];
		}
		delegate.PlayerSwitched();
	}
// switch turn to next player	
	public void switchTurn(){
		Player temp=this.attacker;
		this.attacker=this.opponent;
		this.opponent=temp;
		temp=null;
		new Handler().postDelayed(new Runnable(){
			public void run(){
				delegate.PlayerSwitched();
			}
		}, 1);
		
	}
	public Player manualPlayer(){
		return this.manual;
	}
	public void setManualPlayer(Player manual){
		this.manual=manual;
	}
	
	
//Delegate callback for the events from Player class
	//triggered when setMode is called in Player class
	public void ModeChanged(boolean mode)
	{
		delegate.ModeChanged(mode);
		
	}
	//triggered when attack method is called in Player class
	public void Attacked(int position){
		Boolean temp= position == this.opponent.ship().getPosition();
		this.attacker.setLastAttackSuccess(temp);
		this.opponent.setLastHit(temp);
		this.attacker.setOpponentLastCall(true);
		if (temp){
			this.opponent.ship().decreaseLife(10);
		}
		delegate.UpdateInfoUI(this.attacker.getControlMode(),new int[]{this.attacker.attackAttempts,this.attacker.successAttempts},temp);
		temp=null;
		if (!this.attacker.getControlMode()){
			delegate.HighlightAttackedPosition(position);
			new Handler().postDelayed(new Runnable(){
				public void run(){
				if (self.gameStatus){
					self.switchTurn();
				}
				}
			}, 500);
		}else{
			new Handler().postDelayed(new Runnable(){
				public void run(){
					self.attacker.setCurrentMode(false);
					if (self.gameStatus){
						self.switchTurn();
					}
				}
			}, 300);
		}
		
	}
	//triggered when setPosition is called in Player class
	public void PositionChanged(){
		this.attacker.setOpponentLastCall(false);
		delegate.PositionChanged(this.attacker.ship().getLastPosition(), this.attacker.ship().getPosition());
		new Handler().postDelayed(new Runnable(){
			public void run(){
				self.switchTurn();
			}
		},1000);
	}
	
	
//private helpers
	//Decide the first turn 
	private int decideFirstTurn(int numberOfPlayers){
		Random rand = new Random();
		int max=numberOfPlayers-1;
	    return(rand.nextInt((max - 0) + max) + 0);
	}
	

}
