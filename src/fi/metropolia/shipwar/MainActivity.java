package fi.metropolia.shipwar;




import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;
import fi.metropolia.shipwar.core.GameSessionManager;
import fi.metropolia.shipwar.core.Player;
import fi.metropolia.shipwar.delegates.EventsDelegate;
import fi.metropolia.shipwar.fragments.GameOverFragment;
import fi.metropolia.shipwar.fragments.GamePageFragment;
import fi.metropolia.shipwar.fragments.MainPageFragment;
import fi.metropolia.shipwar.fragments.SelectShipsFragment;
public class MainActivity extends FragmentActivity implements EventsDelegate {
	public GameSessionManager manager;
	private Resources res;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        res=getResources();
        if (findViewById(R.id.fragment_container) != null)
        {
        	if (savedInstanceState != null)
        	{
        		return;
        	}
        	
        	MainPageFragment mainPage=new MainPageFragment();
        	
        	mainPage.setArguments(getIntent().getExtras());
        	getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, mainPage).commit();
        }
    }
	public void onWindowFocusChanged(boolean hasFocus){
		super.onWindowFocusChanged(hasFocus);
	 if (Build.VERSION.SDK_INT >= 19) {
			// if (hasFocus){
			 
		        	View decorView = getWindow().getDecorView();
		        	 decorView.setSystemUiVisibility(
		                     View.SYSTEM_UI_FLAG_LAYOUT_STABLE
		                     | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
		                     | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
		                     | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
		                     | View.SYSTEM_UI_FLAG_FULLSCREEN);
		                   //  | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
		      //  }
		}
	}
	
	public void onBackPressed(){
	/*	 new AlertDialog.Builder(this)
         .setMessage("Are you sure you want to exit?")
         .setCancelable(false)
         .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
             public void onClick(DialogInterface dialog, int id) {
            		MainPageFragment mainPage=new MainPageFragment();
                	mainPage.setArguments(getIntent().getExtras());
                	getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, mainPage).commit();
             }
         })
         .setNegativeButton("No", null)
         .show();
	
	*/super.onBackPressed();
		}
   
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
 //UI Event Handler   
    public void ShowNotifications(String msg){
    	Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }
    
//Event Delegates
    //From MainPageFragment when ship has to be selected
    public void NewGame(){
    	SelectShipsFragment selectShipFragment= new SelectShipsFragment();
    	FragmentTransaction transaction = getSupportFragmentManager().beginTransaction(); 
    	selectShipFragment.setArguments(getIntent().getExtras());
    	transaction.replace(R.id.fragment_container, selectShipFragment);
		transaction.addToBackStack(null);
    	transaction.commit();
    }
    
    //From SelectShipFragment when game is started
    public void StartGame(int imageIndex,String name, String desc,String health,String movement){
    	GamePageFragment gamePageFragment=new GamePageFragment();
    	Bundle bundle= new Bundle();
    		bundle.putString("name",name);
    		bundle.putString("desc",desc);
    		bundle.putString("health", health);
    		bundle.putString("movement",movement);
    		bundle.putInt("shipIndex",imageIndex);
    	gamePageFragment.setArguments(bundle);
    	FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
    		transaction.replace(R.id.fragment_container, gamePageFragment);
    		transaction.addToBackStack(null);
    		transaction.commit();
    		
    }
    //From GamePageFragment when game is over
    public void GameOver(Player winner){
    	GameOverFragment gameOverFragment=new GameOverFragment();
    	Bundle bundle = new Bundle();
    		bundle.putString("player",winner.getName());
    	gameOverFragment.setArguments(bundle);
    	FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
    		transaction.add(R.id.fragment_container, gameOverFragment);
    		transaction.commit();
    }
    
}
