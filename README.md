# **Ship Wars** #
Shipwars is a simple strategy war game for android platform. There are two sides in the game, manual player and the computer. Each player gets its turn one by one. In its turn, player can either change its position in the war grid or attack the opponent, according to the situation. The player who hits the most in the target and drains out the life bar of the opponent wins. 

This was a project I did for a course in my school. 